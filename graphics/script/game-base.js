var current_run_replicant = nodecg.Replicant("current_info", {defaultValue: {}});
current_run_replicant.on("change", (newValue, oldValue) => {
	console.log("run changes!", newValue);
	if (newValue.game_name != "") {
		$("#game").css("visibility", "visible");
		$("#game #name").text(newValue.game_name);
		if (newValue.game_platform != "") {
			$("#game #platform").css("visibility", "visible");
			$("#game #platform").text(newValue.game_platform);
		} else {
			$("#game #platform").css("visibility", "hidden");
		}
	} else {
		$("#game").css("visibility", "hidden");
	}
	for (var runner = 0; runner < 4; runner++) {
		if (runner < newValue.runners.length) {
			$(`#runners #runner${runner+1}`).css("visibility", "visible");
			$(`#runners #runner${runner+1} #name`).text(newValue.runners[runner].name);
			if (newValue.runners[0].pronouns != "") {
				$(`#runners #runner${runner+1} #pronouns`).css("visibility", "visible");
				$(`#runners #runner${runner+1} #pronouns`).text(newValue.runners[runner].pronouns);
			} else {
				$(`#runners #runner${runner+1} #pronouns`).css("visibility", "hidden");
			}
		} else {
			$(`#runners #runner${runner+1}`).css("visibility", "hidden");
			$(`#runners #runner${runner+1} #pronouns`).css("visibility", "hidden");
		}
	}
	var aspect_classes = Array.from($("body")[0].classList).filter((cls) => { return cls.startsWith("aspect-"); });
	for (var cls of aspect_classes) {
		$("body").removeClass(cls);
	}
	$("body").addClass(`aspect-${newValue.game_aspect}`);
});
var current_event_replicant = nodecg.Replicant("event_info", {defaultValue: {}});
current_event_replicant.on("change", (newValue, oldValue) => {
	console.log("event changes!", newValue);
	$("#event-name").text(newValue.name);
});