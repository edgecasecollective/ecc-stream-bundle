'use strict';
const express = require('express');
const app = express();

module.exports = function (nodecg) {
	function runners_as_string(current_run) {
		if (current_run) {
			if (current_run.runners.length == 0) {
				return "There are no currently registered runners!";
			} else if (current_run.runners.length == 1) {
				var output = "The current runner is " + current_run.runners[0].name;
				if (current_run.runners[0].pronouns) {
					output += " (" + current_run.runners[0].pronouns + ")";
				}
				output += "!";
				if (current_run.runners[0].twitch != "") {
					output += " Follow at twitch.tv/" + current_run.runners[0].twitch;
				}
				return output;
			} else {
				var first = true;
				return "The current runners are: " + current_run.runners.map((runner) => {
					var str = "";
					if (runner.twitch != "") {
						str = "https://twitch.tv/" + runner.twitch;
					} else {
						str = runner.name;
					}
					if (runner.pronouns != "") {
						str += " (" + runner.pronouns + ")";
					}
					return str;
				}).join(" ");
			}
		}
	}

	var rotational_bag = [];

	app.get('/query/event', (req, res) => {
		res.json(nodecg.readReplicant("event_info", "ecc-stream-bundle"));
	});
	app.get('/query/run', (req, res) => {
		var run_info = nodecg.readReplicant("current_info", "ecc-stream-bundle");
		run_info.runners_string = runners_as_string(run_info);
		res.json(run_info);
	});
	app.get('/query/rotational', (req, res) => {
		// if the bag is empty, fill it.
		var rotational = nodecg.readReplicant("bot_rotation", "ecc-stream-bundle");
		var event_info = nodecg.readReplicant("event_info", "ecc-stream-bundle");
		var current_info = nodecg.readReplicant("current_info", "ecc-stream-bundle");
		if (rotational_bag.length == 0) {
			// this method of generating the bag is not efficient, but it should never be very large.
			rotational_bag = [];
			for (var i = 0; i < rotational.weights.ecc; i++) {
				rotational_bag.push({which: "ecc", text: "We are Edge Case Collective: a Twitch streaming group of creative queers playing, building, and breaking games in interesting ways! hacks, randos, music, analysis through a queer lens & more!"});
			}
			for (var i = 0; i < rotational.weights.event; i++) {
				if (event_info.description != "") {
					rotational_bag.push({which: "event", text: event_info.description});
				}
			}
			for (var i = 0; i < rotational.weights.run; i++) {
				if (current_info.game_name != "") {
					var str = "The current run is " + current_info.game_name;
					if (current_info.game_platform != "") {
						str += " for " + current_info.game_platform;
					}
					if (current_info.game_estimate != "") {
						str += " with an estimate of " + current_info.game_estimate;
					}
					rotational_bag.push({which: "run", text: str});
				}
			}
			for (var i = 0; i < rotational.weights.runners; i++) {
				rotational_bag.push({which: "runners", text: runners_as_string(current_info)});
			}
			for (var i = 0; i < rotational.weights.other; i++) {
				if (rotational.text.other != "") {
					rotational_bag.push({which: "other", text: rotational.text.other});
				}
			}
		}

		// pick a random thing from the bag
		var choice = Math.floor(Math.random() * rotational_bag.length);
		res.json(rotational_bag[choice]);
		// remove it from the bag
		rotational_bag.splice(choice, 1);
	});

    nodecg.mount(app);
};